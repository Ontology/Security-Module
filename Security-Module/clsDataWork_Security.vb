﻿Imports OntologyAppDBConnector
Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Imports Structure_Module

Public Class clsDataWork_Security
    Private objLocalConfig As clsLocalConfig
    Private objPasswordList As New List(Of clsObjectRel)

    Private objFrmMain As frmMain

    Private objDBLevel_PasswordClasses As OntologyModDBConnector
    Private objDBLevel_Passwords As OntologyModDBConnector
    Private objDBLevel_SecuredBy As OntologyModDBConnector
    Private objDBLevel_AllowedClass As OntologyModDBConnector
    Private objDBLevel_SessionData As OntologyModDBConnector
    Private objDBLevel_OItem As OntologyModDBConnector
    Private objOItem_Result_Passwords As clsOntologyItem
    Private intCountNodes As Integer

    Private objTreeNode_Class As TreeNode
    Private objThread_Passwords As Threading.Thread

    Private objTransaction As clsTransaction
    Private objRelationConfig As clsRelationConfig

    Public Delegate Sub objLoadedPasswords()
    Public Event loadedPasswords As objLoadedPasswords

    Public ReadOnly Property PasswordList
        Get
            Return objPasswordList
        End Get
    End Property

    Public ReadOnly Property CountNodes As Integer
        Get
            Return intCountNodes
        End Get
    End Property

    Public ReadOnly Property OItem_Result_Passwords As clsOntologyItem
        Get
            Return objOItem_Result_Passwords
        End Get
    End Property

    Public Function GetUser(strUserName As String) As clsOntologyItem
        Return GetSecItem(strUserName, objLocalConfig.OItem_type_User)
    End Function

    Public Function GetGroup(strGroupName As String) As clsOntologyItem
        Return GetSecItem(strGroupName, objLocalConfig.OItem_Type_Group)
    End Function

    Private Function GetSecItem(strName As String, objOItem_SecItemClass As clsOntologyItem) As clsOntologyItem
        Dim objSearchSecItem = New List(Of clsOntologyItem) From {New clsOntologyItem With {.Name = strName, .GUID_Parent = objOItem_SecItemClass.GUID}}

        Dim objResult = objDBLevel_OItem.GetDataObjects(objSearchSecItem)

        If objResult.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            Return Nothing
        End If

        Return objDBLevel_OItem.Objects1.FirstOrDefault(Function(objSecItem) objSecItem.Name.ToLower() = strName.ToLower())
    End Function

    Public Function isObject_OK(ByVal OItem_Object As clsOntologyItem) As Boolean
        Dim objOList_Object_To_Password As New List(Of clsClassRel)
        Dim boolOK As Boolean

        objOList_Object_To_Password.Add(New clsClassRel With {.ID_Class_Left = OItem_Object.GUID_Parent,
                                                              .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID,
                                                              .Ontology = objLocalConfig.Globals.Type_Class})

        objDBLevel_AllowedClass.GetDataClassRel(objOList_Object_To_Password, _
                                                  doIds:=True)

        If objDBLevel_AllowedClass.ClassRelsId.Count > 0 Then
            boolOK = True
        Else
            boolOK = False
        End If

        Return boolOK
    End Function

    Private Sub fill_Passwords_Threads()
        Dim objOList_Passwords As New List(Of clsObjectRel)
        Dim objOList_Secured As New List(Of clsObjectRel)

        objPasswordList.Clear()

        objOList_Passwords.Add(New clsObjectRel With {.ID_Parent_Object = objTreeNode_Class.Name,
                                                      .ID_Other = objLocalConfig.OItem_User.GUID,
                                                      .ID_RelationType = objLocalConfig.OItem_RelationType_encoded_by.GUID})
            
        objDBLevel_Passwords.GetDataObjectRel(objOList_Passwords, _
                                                doIds:=False)

        objOList_Secured.Add(New clsObjectRel With {.ID_Parent_Other = objTreeNode_Class.Name,
                                                    .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID})
            
        objDBLevel_SecuredBy.GetDataObjectRel(objOList_Secured, _
                                                doIds:=False)

        objPasswordList = (From objPassword In objDBLevel_Passwords.ObjectRels
                           Join objSecured In objDBLevel_SecuredBy.ObjectRels On objPassword.ID_Object Equals objSecured.ID_Other
                           Select objSecured).ToList()

        objOItem_Result_Passwords = objLocalConfig.Globals.LState_Success
        RaiseEvent loadedPasswords()
    End Sub

    Public Function IsItemSecured(objOItem_Item As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result = objLocalConfig.Globals.LState_Success.Clone()
        Dim objOList_Passwords As New List(Of clsObjectRel)
        Dim objOList_Secured As New List(Of clsObjectRel)

        objOList_Passwords.Add(New clsObjectRel With {.ID_Parent_Object = objTreeNode_Class.Name,
                                                      .ID_Other = objLocalConfig.OItem_User.GUID,
                                                      .ID_RelationType = objLocalConfig.OItem_RelationType_encoded_by.GUID})
            
        objOItem_Result = objDBLevel_Passwords.GetDataObjectRel(objOList_Passwords, _
                                               doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOList_Secured = objDBLevel_Passwords.ObjectRels.Select(Function(pwd) New clsObjectRel With {.ID_Other = pwd.ID_Object,
                                                                                                            .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID,
                                                                                                            .ID_Object = objOItem_Item.GUID}).ToList()

            If objOList_Secured.Any() Then
                objOItem_Result = objDBLevel_SecuredBy.GetDataObjectRel(objOList_Secured, _
                                                    doIds:=False)

                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                    If Not objDBLevel_SecuredBy.ObjectRels.Any() Then
                        objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone()
                    End If
                End If
            Else
                objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone()
            End If

        End If


        Return objOItem_Result
    End Function

    Public Sub fill_PasswordNodes(ByVal objTreeNode_Root As TreeNode)
        Dim objOList_Nodes As New List(Of clsObjectRel)
        Dim objObjRel_Encoded As clsObjectRel
        Dim objTreeNode As TreeNode

        intCountNodes = 0

        objOList_Nodes.Add(New clsObjectRel With {.ID_Object = objLocalConfig.OItem_BaseConfig.GUID,
                                                  .ID_RelationType = objLocalConfig.OItem_RelationType_belonging_Endoding_Types.GUID})
            
        objDBLevel_PasswordClasses.GetDataObjectRel(objOList_Nodes, _
                                                      doIds:=False)

        If objDBLevel_PasswordClasses.ObjectRels.Any() Then
            For Each objObjRel_Encoded In objDBLevel_PasswordClasses.ObjectRels
                objTreeNode = objTreeNode_Root.Nodes.Add(objObjRel_Encoded.ID_Other, _
                                                         objObjRel_Encoded.Name_Other, _
                                                         objLocalConfig.ImageID_Type_Passwords_Closed, _
                                                         objLocalConfig.ImageID_Type_Passwords_Opened)
                intCountNodes = intCountNodes + 1
            Next
        Else
            MsgBox("Wählen Sie die Klasse, deren Objekte verschlüsselt werden sollen. Die Klasse wird mit der Klasse User verknüpft.", MsgBoxStyle.Information)

            objFrmMain = New frmMain(objLocalConfig.Globals)
            objFrmMain.Applyable = True
            objFrmMain.ShowDialog()

            Dim objOItem_ClassEncryption As clsOntologyItem = Nothing

            objTransaction.ClearItems()
            If objFrmMain.DialogResult = DialogResult.OK Then
                If objFrmMain.OList_Simple.Count = 1 Then
                    If objFrmMain.OList_Simple(0).Type = objLocalConfig.Globals.Type_Class Then
                        objOItem_ClassEncryption = objFrmMain.OList_Simple(0)

                        If Not objOItem_ClassEncryption.GUID = objLocalConfig.OItem_type_User.GUID Then
                            Dim objORel_ClassRel = objRelationConfig.Rel_ClassRelation(objLocalConfig.OItem_type_User,
                                                                                       objOItem_ClassEncryption,
                                                                                       objLocalConfig.OItem_RelationType_secured_by,
                                                                                       0, 1, 1)
                            If Not objORel_ClassRel Is Nothing Then
                                Dim objOItem_Result = objTransaction.do_Transaction(objORel_ClassRel, True)
                                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                    Dim objORel_ObjRel = objRelationConfig.Rel_ObjectRelation(objLocalConfig.OItem_BaseConfig,
                                                                                                objOItem_ClassEncryption,
                                                                                                objLocalConfig.OItem_RelationType_belonging_Endoding_Types)

                                    objOItem_Result = objTransaction.do_Transaction(objORel_ObjRel, True)
                                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                        fill_PasswordNodes(objTreeNode_Root)
                                    Else

                                        objTransaction.rollback()
                                    End If
                                Else

                                    objOItem_ClassEncryption = Nothing
                                    MsgBox("Die Klassen konnten nicht verknüpft werden!", MsgBoxStyle.Exclamation)
                                End If

                            Else
                                objOItem_ClassEncryption = Nothing
                                MsgBox("Die Klassen konnten nicht verknüpft werden!", MsgBoxStyle.Exclamation)
                            End If
                        Else
                            objOItem_ClassEncryption = Nothing
                            MsgBox("Sie müssen eine andere Klasse als die User-Klasse auswählen!", MsgBoxStyle.Information)
                        End If

                    Else
                        MsgBox("Sie müssen eine Klasse auswählen!", MsgBoxStyle.Information)

                    End If
                Else
                    MsgBox("Sie müssen eine Klasse auswählen!", MsgBoxStyle.Information)
                End If

            End If

            If objOItem_ClassEncryption Is Nothing Then
                Environment.Exit(-1)
            End If
        End If


    End Sub

    Public Sub fill_passwords(ByVal TreeNode_Class As TreeNode)
        objTreeNode_Class = TreeNode_Class

        objOItem_Result_Passwords = objLocalConfig.Globals.LState_Nothing

        Try
            objThread_Passwords.Abort()
        Catch ex As Exception

        End Try

        objThread_Passwords = New Threading.Thread(AddressOf fill_Passwords_Threads)
        objThread_Passwords.Start()

    End Sub

    Public Function GetSessionData() As List(Of clsObjectRel)
        Dim objORel_SessionRef = New List(Of clsObjectRel) From {New clsObjectRel With {.Name_Object = objLocalConfig.Globals.Session,
                                                                                         .ID_Parent_Object = objLocalConfig.OItem_class_security_session.GUID,
                                                                                         .ID_Parent_Other = objLocalConfig.OItem_type_User.GUID}}

        objORel_SessionRef.Add(New clsObjectRel With {.Name_Object = objLocalConfig.Globals.Session,
                                                                                         .ID_Parent_Object = objLocalConfig.OItem_class_security_session.GUID,
                                                                                         .ID_Parent_Other = objLocalConfig.OItem_Type_Group.GUID})

        objORel_SessionRef.Add(New clsObjectRel With {.Name_Object = objLocalConfig.Globals.Session,
                                                                                         .ID_Parent_Object = objLocalConfig.OItem_class_security_session.GUID,
                                                                                         .ID_Parent_Other = objLocalConfig.OItem_class_server.GUID})

        Dim objOItem_Result = objDBLevel_SessionData.GetDataObjectRel(objORel_SessionRef, doIds:=False)

        If (objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID) Then
            Return objDBLevel_SessionData.ObjectRels
        Else
            Return Nothing
        End If
    End Function

    Public Function GetPasswordOfUser(objOItem_User As clsOntologyItem) As clsOntologyItem
        Dim objSearchPassword = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Object = objOItem_User.GUID,
            .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID
        }}

        Dim objOItem_Result = objDBLevel_Passwords.GetDataObjectRel(objSearchPassword)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            Return objDBLevel_Passwords.ObjectRels.Select(Function(userRelation) New clsOntologyItem With {.GUID = userRelation.ID_Other,
                                                              .Name = userRelation.Name_Other,
                                                              .GUID_Parent = userRelation.ID_Parent_Other,
                                                              .Type = userRelation.Ontology}).FirstOrDefault()
        Else
            Return Nothing
        End If
    End Function

    Public Sub New(ByVal LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig
        set_DBConnection()
    End Sub

    Private Sub set_DBConnection()
        objDBLevel_PasswordClasses = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Passwords = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_SecuredBy = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_AllowedClass = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_SessionData = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_OItem = New OntologyModDBConnector(objLocalConfig.Globals)

        objTransaction = New clsTransaction(objLocalConfig.Globals)
        objRelationConfig = New clsRelationConfig(objLocalConfig.Globals)
    End Sub
End Class
