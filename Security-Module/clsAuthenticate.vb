﻿Imports System.Reflection
Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices

Public Class clsAuthenticate
    Private objLocalConfig As clsLocalConfig
    Private boolGroup As Boolean
    Private boolUser As Boolean
    Private intAuthenticationMode As Integer
    Private boolUseSessionData As Boolean
    Public Property objOItem_User As clsOntologyItem
    Public Property objOItem_Group As clsOntologyItem
    Private objFrmAuthenticate As frmAuthenticate
    Private objOItem_Module As clsOntologyItem
    Private objDataWork_Security As clsDataWork_Security
    Private sessionList As List(Of clsObjectRel)

    Public Sub New(ByVal Globals As Globals)
        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If


        initialize()
    End Sub

    Public Function Authenticate(ByVal boolUser As Boolean, ByVal boolGroup As Boolean, ByVal AuthenticationMode As frmAuthenticate.ERelateMode, Optional boolUseSessionData As Boolean = False, Optional OItem_Module As clsOntologyItem = Nothing) As clsOntologyItem
        intAuthenticationMode = AuthenticationMode
        Me.boolGroup = boolGroup
        Me.boolUser = boolUser
        set_DBConnection()
        objLocalConfig.OItem_ModuleForSession = OItem_Module
        Me.boolUseSessionData = boolUseSessionData
        Me.objOItem_Module = OItem_Module

        If boolUseSessionData Then
            objDataWork_Security = New clsDataWork_Security(objLocalConfig)
            Dim objOItem_Result = GetSessionData()
            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                sessionList = New List(Of clsObjectRel)
                Return objLocalConfig.Globals.LState_Error.Clone()
            Else
                Dim objUsersOfSessions = sessionList.Where(Function(s) s.ID_Parent_Other = objLocalConfig.OItem_type_User.GUID).ToList()
                Dim objGroupsOfSessions = sessionList.Where(Function(s) s.ID_Parent_Other = objLocalConfig.OItem_Type_Group.GUID).ToList()
                Dim objSessionsOfServer = sessionList.Where(Function(s) s.ID_Other = objLocalConfig.Globals.OItem_Server.GUID).ToList()

                If objLocalConfig.OItem_ModuleForSession Is Nothing Then
                    Dim objUsers = (From objUserSession In objUsersOfSessions
                                           Join objServerSession In objSessionsOfServer On objUserSession.ID_Object Equals objServerSession.ID_Object
                                           Select New clsOntologyItem With {.GUID = objUserSession.ID_Other, _
                                                                            .Name = objUserSession.Name_Other, _
                                                                            .GUID_Parent = objUserSession.ID_Parent_Other, _
                                                                            .Type = objLocalConfig.Globals.Type_Object}).ToList()

                    Dim objGroups = (From objGroupSession In objGroupsOfSessions
                                           Join objServerSession In objSessionsOfServer On objGroupSession.ID_Object Equals objServerSession.ID_Object
                                           Select New clsOntologyItem With {.GUID = objGroupSession.ID_Other, _
                                                                            .Name = objGroupSession.Name_Other, _
                                                                            .GUID_Parent = objGroupSession.ID_Parent_Other, _
                                                                            .Type = objLocalConfig.Globals.Type_Object}).ToList()

                    If objUsers.Any() Then
                        objOItem_User = objUsers.First
                    End If

                    If objGroups.Any() Then
                        objOItem_Group = objGroups.First
                    End If

                Else
                    Dim objSessionsOfModule = sessionList.Where(Function(s) s.ID_Other = objLocalConfig.OItem_ModuleForSession.GUID).ToList()
                    If objSessionsOfModule.Any() Then
                        Dim objUsers = (From objUserSession In objUsersOfSessions
                                    Join objModuleSession In objSessionsOfModule On objUserSession.ID_Object Equals objModuleSession.ID_Object
                                           Join objServerSession In objSessionsOfServer On objUserSession.ID_Object Equals objServerSession.ID_Object
                                           Select New clsOntologyItem With {.GUID = objUserSession.ID_Other, _
                                                                            .Name = objUserSession.Name_Other, _
                                                                            .GUID_Parent = objUserSession.ID_Parent_Other, _
                                                                            .Type = objLocalConfig.Globals.Type_Object}).ToList()

                        Dim objGroups = (From objGroupSession In objGroupsOfSessions
                                         Join objModuleSession In objSessionsOfModule On objGroupSession.ID_Object Equals objModuleSession.ID_Object
                                               Join objServerSession In objSessionsOfServer On objGroupSession.ID_Object Equals objServerSession.ID_Object
                                               Select New clsOntologyItem With {.GUID = objGroupSession.ID_Other, _
                                                                                .Name = objGroupSession.Name_Other, _
                                                                                .GUID_Parent = objGroupSession.ID_Parent_Other, _
                                                                                .Type = objLocalConfig.Globals.Type_Object}).ToList()

                        If objUsers.Any() Then
                            objOItem_User = objUsers.First
                        End If

                        If objGroups.Any() Then
                            objOItem_Group = objGroups.First
                        End If
                    Else
                        Dim objUsers = (From objUserSession In objUsersOfSessions
                                           Join objServerSession In objSessionsOfServer On objUserSession.ID_Object Equals objServerSession.ID_Object
                                           Select New clsOntologyItem With {.GUID = objUserSession.ID_Other, _
                                                                            .Name = objUserSession.Name_Other, _
                                                                            .GUID_Parent = objUserSession.ID_Parent_Other, _
                                                                            .Type = objLocalConfig.Globals.Type_Object}).ToList()

                        Dim objGroups = (From objGroupSession In objGroupsOfSessions
                                               Join objServerSession In objSessionsOfServer On objGroupSession.ID_Object Equals objServerSession.ID_Object
                                               Select New clsOntologyItem With {.GUID = objGroupSession.ID_Other, _
                                                                                .Name = objGroupSession.Name_Other, _
                                                                                .GUID_Parent = objGroupSession.ID_Parent_Other, _
                                                                                .Type = objLocalConfig.Globals.Type_Object}).ToList()


                        If objUsers.Any() Then
                            objOItem_User = objUsers.First
                        End If

                        If objGroups.Any() Then
                            objOItem_Group = objGroups.First
                        End If
                    End If

                End If
                If (boolUser And objOItem_User Is Nothing) Or
                    (boolGroup And objOItem_Group Is Nothing) Then

                    objFrmAuthenticate = New frmAuthenticate(objLocalConfig, boolUser, boolGroup, intAuthenticationMode, boolUseSessionData, objOItem_Module)
                    objFrmAuthenticate.ShowDialog()
                    If objFrmAuthenticate.DialogResult = DialogResult.OK Then
                        objOItem_User = objFrmAuthenticate.OItem_User
                        objOItem_Group = objFrmAuthenticate.OItem_Group
                        Return objLocalConfig.Globals.LState_Success.Clone()
                    Else
                        Return objLocalConfig.Globals.LState_Nothing.Clone()
                    End If
                Else
                    Return objLocalConfig.Globals.LState_Success.Clone()
                End If

            End If
        Else
            objFrmAuthenticate = New frmAuthenticate(objLocalConfig, boolUser, boolGroup, intAuthenticationMode, boolUseSessionData, objOItem_Module)
            objFrmAuthenticate.ShowDialog()
            If objFrmAuthenticate.DialogResult = DialogResult.OK Then
                objOItem_User = objFrmAuthenticate.OItem_User
                objOItem_Group = objFrmAuthenticate.OItem_Group
                Return objLocalConfig.Globals.LState_Success.Clone()
            Else
                Return objLocalConfig.Globals.LState_Nothing.Clone()
            End If
        End If
    End Function

    Private Sub initialize()

    End Sub

    Private Sub set_DBConnection()

    End Sub

    Private Function GetSessionData() As clsOntologyItem
        sessionList = objDataWork_Security.GetSessionData()

        If Not sessionList Is Nothing Then
            Return objLocalConfig.Globals.LState_Success.Clone
        Else
            Return objLocalConfig.Globals.LState_Error.Clone
        End If
    End Function
End Class
