﻿Imports System.Reflection
Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Imports Structure_Module
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices
Imports System.Threading

Public Class frmMenu

    Private objLocalConfig As clsLocalConfig
    Private objDataWork_Menu As clsDataWork_Menu
    Private objOItem_Ref As clsOntologyItem
    Private objOItem_Class As clsOntologyItem
    Private objSecurityWork As clsSecurityWork
    Private objTransaction As clsTransaction
    Private strPassword As String

    Private objFrm_Name As frm_Name

    Private Delegate Sub ActivateRefItemDelegate()
    Private ActiveRefItemHandler As ActivateRefItemDelegate

    Private objRelationConfig As clsRelationConfig

    Private threadModExchangeServer As Thread

    Private objFrmAdvancedFilter As frmAdvancedFilter

    Public Sub New(Globals As Globals, OItem_Ref As clsOntologyItem, OItem_User As clsOntologyItem)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        objOItem_Ref = OItem_Ref
        objSecurityWork = New clsSecurityWork(objLocalConfig, Me)
        objSecurityWork.initialize_User(OItem_User)

        Initialize()
    End Sub

    Public Sub New(LocalConfig As clsLocalConfig, OItem_Ref As clsOntologyItem, SecurityWork As clsSecurityWork)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objLocalConfig = LocalConfig
        objOItem_Ref = OItem_Ref
        objSecurityWork = SecurityWork

        Initialize()
    End Sub

    Private Sub Initialize()
        ActiveRefItemHandler = New ActivateRefItemDelegate(AddressOf ActiveRefItem)
        objDataWork_Menu = New clsDataWork_Menu(objLocalConfig)
        objTransaction = New clsTransaction(objLocalConfig.Globals)
        objRelationConfig = New clsRelationConfig(objLocalConfig.Globals)

        Me.Text = ""

        If objOItem_Ref.Type.ToLower() = objLocalConfig.Globals.Type_Object.ToLower() Then
            objOItem_Class = objDataWork_Menu.GetOItem(objOItem_Ref.GUID_Parent, objLocalConfig.Globals.Type_Class)
            If Not objOItem_Class Is Nothing Then
                Me.Text = objOItem_Class.Name & "\"
            End If
        End If


        Me.Text = Me.Text & objOItem_Ref.Name

        FillGrid()
    End Sub

    Private Sub FillGrid()
        Dim objOItem_Result = objDataWork_Menu.GetSecuredItems(objOItem_Ref)

        DataGridView_Secured.DataSource = Nothing
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            DataGridView_Secured.DataSource = New SortableBindingList(Of clsSecuredItem)(objDataWork_Menu.SecuredItems)
            For Each col As DataGridViewColumn In DataGridView_Secured.Columns
                If col.Name = "Password" Or
                   col.Name = "Name_RelationType" Or
                   col.Name = "Name_User" Then

                    col.Visible = True
                Else
                    col.Visible = False
                End If

            Next
        Else
            MsgBox("Die Daten konnten nicht geladen werden! Die Anwendung wird beendet.", MsgBoxStyle.Exclamation)
            Environment.Exit(-1)
        End If
    End Sub

    Private Sub ContextMenuStrip_Passwords_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip_Passwords.Opening
        NewToolStripMenuItem.Enabled = True
        ChangeToolStripMenuItem.Enabled = False
        CopyPasswordToolStripMenuItem.Enabled = False

        If DataGridView_Secured.SelectedRows.Count = 1 Then
            ChangeToolStripMenuItem.Enabled = True

            Dim dataRow As DataGridViewRow = DataGridView_Secured.SelectedRows(0)
            Dim securedItem As clsSecuredItem = dataRow.DataBoundItem

            If Not securedItem.Name_Password Is Nothing Then
                CopyPasswordToolStripMenuItem.Enabled = True
            End If

        End If
    End Sub

    Private Sub CopyPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopyPasswordToolStripMenuItem.Click
        If DataGridView_Secured.SelectedRows.Count = 1 Then

            Dim dataRow As DataGridViewRow = DataGridView_Secured.SelectedRows(0)
            Dim securedItem As clsSecuredItem = dataRow.DataBoundItem

            If Not securedItem.Name_Password Is Nothing Then
                strPassword = objSecurityWork.decode_Password(securedItem.Name_Password)
                Clipboard.SetDataObject(strPassword)
                Timer_Copy.Start()
            End If

        End If
    End Sub

    Private Sub Timer_Copy_Tick(sender As Object, e As EventArgs) Handles Timer_Copy.Tick
        Timer_Copy.Stop()
        strPassword = ""
        Clipboard.Clear()
    End Sub

    Private Sub frmMenu_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Clipboard.Clear()
    End Sub

    Private Sub ModExchangeHandler()
        AddHandler ModuleDataExchanger._serverResponse, AddressOf ExchangeAsyncHandler
        ModuleDataExchanger.Server(objLocalConfig)

    End Sub

    Private Sub ActiveRefItem()
        If InvokeRequired Then
            Invoke(ActiveRefItemHandler)
        Else
            Initialize()
        End If
    End Sub


    Private Sub ExchangeAsyncHandler(result As String)
        If Not String.IsNullOrEmpty(result) Then
            Dim objArgumentParsing = New clsArgumentParsing(objLocalConfig.Globals, result.Split(" ").ToList(), True)
            If objArgumentParsing.OList_Items.Count = 1 Then
                If objArgumentParsing.OList_Items.First().Type.ToLower() = objLocalConfig.Globals.Type_Object.ToLower() Then
                    objOItem_Ref = objArgumentParsing.OList_Items.First()
                    ActiveRefItem()

                End If
            End If
        End If
        ModExchangeHandler()
    End Sub

    Private Sub ListenToolStripMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles ListenToolStripMenuItem.CheckStateChanged
        If (ListenToolStripMenuItem.Checked) Then
            Try
                threadModExchangeServer.Abort()
                ModuleDataExchanger.Disconnect()
            Catch ex As Exception

            End Try
            threadModExchangeServer = New Thread(AddressOf ModExchangeHandler)
            threadModExchangeServer.Start()
        Else
            Try
                threadModExchangeServer.Abort()
                ModuleDataExchanger.Disconnect()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub ChangeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChangeToolStripMenuItem.Click
        Dim objDGVR_Selected As DataGridViewRow
        Dim objSecuredItem As clsSecuredItem
        Dim strPassword_Decoded As String
        Dim strPassword_Encoded As String
        Dim objOItem_Result As clsOntologyItem
        Dim objOItem_Password As clsOntologyItem
        Dim objOItem_Class_Password As clsOntologyItem
        Dim objOItem_User As clsOntologyItem

        objOItem_Result = objDataWork_Menu.GetPasswordClass()

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            If Not objOItem_Result.OList_Rel Is Nothing Then

                If objOItem_Result.OList_Rel.Any() Then

                    objOItem_Class_Password = objOItem_Result.OList_Rel.First()
                    objDGVR_Selected = DataGridView_Secured.SelectedRows(0)
                    objSecuredItem = CType(objDGVR_Selected.DataBoundItem, clsSecuredItem)

                    If Not String.IsNullOrEmpty(objSecuredItem.GUID_User) Then
                        objOItem_User = New clsOntologyItem With {.GUID = objSecuredItem.GUID_User,
                                                                  .Name = objSecuredItem.Name_User,
                                                                  .GUID_Parent = objLocalConfig.OItem_type_User.GUID,
                                                                  .Type = objLocalConfig.Globals.Type_Object}
                        objFrm_Name = New frm_Name("New Password", _
                                                           objLocalConfig.Globals, _
                                                           isSecured:=True, _
                                                           showRepeat:=True)
                        objFrm_Name.ShowDialog(Me)

                        If objFrm_Name.DialogResult = Windows.Forms.DialogResult.OK Then
                            objOItem_Password = New clsOntologyItem
                            If Not String.IsNullOrEmpty(objSecuredItem.GUID_Password) Then
                                objOItem_Password.GUID = objSecuredItem.GUID_Password

                            Else
                                objOItem_Password.GUID = objLocalConfig.Globals.NewGUID

                            End If
                            objOItem_Password.GUID_Parent = objOItem_Class_Password.GUID

                            objOItem_Password.Type = objLocalConfig.Globals.Type_Object



                            strPassword_Decoded = objFrm_Name.Value1

                            strPassword_Encoded = objSecurityWork.encode_Password(strPassword_Decoded)

                            objOItem_Password.Name = strPassword_Encoded

                            objTransaction.ClearItems()
                            objOItem_Result = objTransaction.do_Transaction(objOItem_Password)

                            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                If String.IsNullOrEmpty(objSecuredItem.GUID_Password) Then
                                    Dim objORel_User_To_Password = objRelationConfig.Rel_ObjectRelation(objOItem_User, objOItem_Password, objLocalConfig.OItem_RelationType_secured_by)
                                    objOItem_Result = objTransaction.do_Transaction(objORel_User_To_Password)
                                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                        MsgBox("Das Passwort wurde erzeugt!", MsgBoxStyle.Exclamation)

                                    Else
                                        objTransaction.rollback()
                                        MsgBox("Das Passwort konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                                    End If

                                Else
                                    MsgBox("Das Passwort wurde geändert!", MsgBoxStyle.Exclamation)
                                End If
                                FillGrid()
                            Else
                                MsgBox("Das Passwort konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                            End If



                        End If
                    Else
                        MsgBox("Dem Refitem ist kein User zugeordnet!", MsgBoxStyle.Exclamation)
                    End If



                Else
                    MsgBox("Die Passwort-Klasse kann nicht ermittelt werden!", MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("Die Passwort-Klasse kann nicht ermittelt werden!", MsgBoxStyle.Exclamation)
            End If
        Else
            MsgBox("Die Passwort-Klasse kann nicht ermittelt werden!", MsgBoxStyle.Exclamation)
        End If

        
        
    End Sub

    Private Sub NewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewToolStripMenuItem.Click
        Dim strPassword_Decoded As String
        Dim strPassword_Encoded As String

        Dim objOItem_ObjectForPassword As clsOntologyItem = Nothing
        Dim objOItem_ClassOfObject As clsOntologyItem
        Dim objOItem_RelationTypeForPassword As clsOntologyItem
        Dim objOItem_Direction As clsOntologyItem
        Dim objOItem_Password As New clsOntologyItem
        Dim objOItem_Secured As New clsOntologyItem
        Dim objOItem_Result As clsOntologyItem
        Dim objOItem_ClassOfPassword As clsOntologyItem


        If objOItem_Class.GUID = objLocalConfig.OItem_type_User.GUID Then
            objOItem_ObjectForPassword = objOItem_Ref
        Else
            objFrmAdvancedFilter = New frmAdvancedFilter(objLocalConfig.Globals, objOItem_Class)
            objFrmAdvancedFilter.ShowDialog(Me)
            If objFrmAdvancedFilter.DialogResult = DialogResult.OK Then
                If objFrmAdvancedFilter.OItem_Object IsNot Nothing And
                    objFrmAdvancedFilter.OItem_RelationType IsNot Nothing And
                    objFrmAdvancedFilter.OItem_Direction IsNot Nothing And
                    objFrmAdvancedFilter.OItem_Class IsNot Nothing Then

                    objOItem_ObjectForPassword = objFrmAdvancedFilter.OItem_Object
                    objOItem_RelationTypeForPassword = objFrmAdvancedFilter.OItem_RelationType
                    objOItem_Direction = objFrmAdvancedFilter.OItem_Direction
                    objOItem_ClassOfObject = objFrmAdvancedFilter.OItem_Class
                Else
                    MsgBox("Bitte wählen Sie ein Objekt aus, welchem ein Password zugeordnet werden soll.")
                End If
            End If
        End If

        If objOItem_ObjectForPassword IsNot Nothing Then

            objOItem_ClassOfPassword = objDataWork_Menu.GetRelatedSecurityClass(objOItem_ClassOfObject)
            If objOItem_ClassOfPassword IsNot Nothing Then

                If objDataWork_Menu.IsItemSecured(objOItem_ObjectForPassword).GUID = objLocalConfig.Globals.LState_Nothing.GUID Then
                    objFrm_Name = New frm_Name(objOItem_ClassOfObject.Name & "\" & objOItem_ObjectForPassword.Name,
                        objLocalConfig.Globals,
                        isSecured:=True,
                        showRepeat:=True)
                    objFrm_Name.ShowDialog(Me)
                    If objFrm_Name.DialogResult = DialogResult.OK Then
                        strPassword_Decoded = objFrm_Name.Value1
                        strPassword_Encoded = objSecurityWork.encode_Password(strPassword_Decoded)

                        objOItem_Password = New clsOntologyItem
                        objOItem_Password.GUID = Guid.NewGuid.ToString.Replace("-", "")
                        objOItem_Password.Name = strPassword_Encoded
                        objOItem_Password.GUID_Parent = objOItem_ClassOfPassword.GUID
                        objOItem_Password.Type = objLocalConfig.Globals.Type_Object

                        objTransaction.ClearItems()
                        objOItem_Result = objTransaction.do_Transaction(objOItem_Password)

                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            Dim objORel = objRelationConfig.Rel_ObjectRelation(objOItem_Password, objLocalConfig.OItem_User, objLocalConfig.OItem_RelationType_encoded_by)

                            objOItem_Result = objTransaction.do_Transaction(objORel)
                            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                objORel = objRelationConfig.Rel_ObjectRelation(objOItem_ObjectForPassword, objOItem_Password, objLocalConfig.OItem_RelationType_secured_by)
                                objOItem_Result = objTransaction.do_Transaction(objORel)
                                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                    Dim objOItem_Left As clsOntologyItem
                                    Dim objOItem_Right As clsOntologyItem

                                    If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                                        objOItem_Left = objOItem_Ref
                                        objOItem_Right = objOItem_ObjectForPassword
                                    Else
                                        objOItem_Left = objOItem_ObjectForPassword
                                        objOItem_Right = objOItem_Ref
                                    End If
                                    objORel = objRelationConfig.Rel_ObjectRelation(objOItem_Left, objOItem_Right, objOItem_RelationTypeForPassword)
                                    objOItem_Result = objTransaction.do_Transaction(objORel)

                                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                        FillGrid()
                                    End If


                                Else
                                    objTransaction.rollback()

                                    MsgBox("Das Passwort kann nicht gespeichert werden!", MsgBoxStyle.Exclamation)

                                End If
                            Else
                                objTransaction.rollback()
                                MsgBox("Das Passwort kann nicht gespeichert werden!", MsgBoxStyle.Exclamation)
                            End If
                        Else
                            MsgBox("Das Passwort kann nicht gespeichert werden!", MsgBoxStyle.Exclamation)
                        End If
                    End If
                Else
                    MsgBox("Das Item ist bereits mit einem ""Passwort"" versehen.", MsgBoxStyle.Information)
                End If

            Else
                MsgBox("Die Klasse des gewählten Objects ist nicht erlaubt!", MsgBoxStyle.Exclamation)
            End If
        End If



    End Sub
End Class