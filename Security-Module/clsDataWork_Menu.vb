﻿Imports OntologyAppDBConnector
Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Public Class clsDataWork_Menu
    Private objLocalConfig As clsLocalConfig

    Private objOItem_Ref As clsOntologyItem

    Private objDBLevel_OItem As OntologyModDBConnector
    Private objDBLevel_User As OntologyModDBConnector
    Private objDBLevel_Password As OntologyModDBConnector
    Private objDBLevel_SecuredBy As OntologyModDBConnector
    Private objDBLevel_AllowedClass As OntologyModDBConnector

    Public Property SecuredItems As List(Of clsSecuredItem)

    Public Function isObject_OK(ByVal OItem_Object As clsOntologyItem) As Boolean
        Dim objOList_Object_To_Password As New List(Of clsClassRel)
        Dim boolOK As Boolean

        objOList_Object_To_Password.Add(New clsClassRel With {.ID_Class_Left = OItem_Object.GUID_Parent,
                                                              .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID,
                                                              .Ontology = objLocalConfig.Globals.Type_Class})

        objDBLevel_AllowedClass.GetDataClassRel(objOList_Object_To_Password,
                                                  doIds:=True)

        If objDBLevel_AllowedClass.ClassRelsId.Count > 0 Then
            boolOK = True
        Else
            boolOK = False
        End If

        Return boolOK
    End Function

    Public Function IsItemSecured(objOItem_Item As clsOntologyItem) As clsOntologyItem


        Dim searchSecuredBy = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Object = objOItem_Item.GUID,
            .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID}}

        Dim objOItem_Result = objDBLevel_SecuredBy.GetDataObjectRel(searchSecuredBy, doIds:=True)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            searchSecuredBy = objDBLevel_SecuredBy.ObjectRelsId.Select(Function(objRel) New clsObjectRel With {.ID_Object = objRel.ID_Other,
                                                                       .ID_RelationType = objLocalConfig.OItem_RelationType_encoded_by.GUID,
                                                                       .ID_Other = objLocalConfig.OItem_User.GUID}).ToList()
            If Not searchSecuredBy.Any() Then
                objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone()
            End If
        End If

        Return objOItem_Result
    End Function

    Public Function GetRelatedSecurityClass(objOItem_Class As clsOntologyItem) As clsOntologyItem
        Dim searchSecuredBy = New List(Of clsClassRel) From {New clsClassRel With {.ID_Class_Left = objOItem_Class.GUID,
            .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID}}

        Dim objOItem_SecurityClass As clsOntologyItem = Nothing
        Dim objOItem_Result = objDBLevel_SecuredBy.GetDataClassRel(searchSecuredBy)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOItem_SecurityClass = objDBLevel_SecuredBy.ClassRels.Select(Function(objClsRel) New clsOntologyItem With {.GUID = objClsRel.ID_Class_Right,
                                                                                   .Name = objClsRel.Name_Class_Right,
                                                                                   .Type = objLocalConfig.Globals.Type_Class}).FirstOrDefault
        End If

        Return objOItem_SecurityClass
    End Function

    Public Function GetSecuredItems(OItem_Ref As clsOntologyItem) As clsOntologyItem
        objOItem_Ref = OItem_Ref

        SecuredItems = New List(Of clsSecuredItem)()

        Dim searchUsers = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Object = objOItem_Ref.GUID, .ID_Parent_Other = objLocalConfig.OItem_type_User.GUID}}
        Dim searchPasswords = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID}}

        Dim objOItem_Result = objDBLevel_User.GetDataObjectRel(searchUsers, doIds:=False)
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOItem_Result = objDBLevel_Password.GetDataObjectRel(searchPasswords, doIds:=False)
            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                SecuredItems = (From objUser In objDBLevel_User.ObjectRels
                                Group Join objPassword In objDBLevel_Password.ObjectRels On objUser.ID_Other Equals objPassword.ID_Object Into objPasswords = Group
                                From objPassword In objPasswords.DefaultIfEmpty()
                                Select New clsSecuredItem With {.GUID_Ref = objOItem_Ref.GUID,
                                                                .Name_Ref = objOItem_Ref.Name,
                                                                .GUID_User = objUser.ID_Other,
                                                                .Name_User = objUser.Name_Other,
                                                                .GUID_Password = If(Not objPassword Is Nothing, objPassword.ID_Other, Nothing),
                                                                .Name_Password = If(Not objPassword Is Nothing, objPassword.Name_Other, Nothing),
                                                                .GUID_Parent_Password = If(Not objPassword Is Nothing, objPassword.ID_Parent_Other, Nothing),
                                                                .Password = If(Not objPassword Is Nothing, "****", Nothing),
                                                                .GUID_RelationType = objUser.ID_RelationType,
                                                                .Name_RelationType = objUser.Name_RelationType}).ToList()

                SecuredItems.AddRange(objDBLevel_Password.ObjectRels.Where(Function(pwd) pwd.ID_Object = objOItem_Ref.GUID).Select(Function(pwd) New clsSecuredItem With {
                                          .GUID_Ref = objOItem_Ref.GUID,
                                          .Name_Ref = objOItem_Ref.Name,
                                          .GUID_RelationType = pwd.ID_RelationType,
                                          .Name_RelationType = pwd.Name_RelationType,
                                          .GUID_Password = pwd.ID_Other,
                                          .Name_Password = pwd.Name_Other,
                                          .Password = "****"}))

            End If
        End If

        Return objOItem_Result
    End Function

    Public Function GetPasswordClass() As clsOntologyItem
        Dim searchPasswordClass = New List(Of clsClassRel) From {New clsClassRel With {.ID_Class_Left = objLocalConfig.OItem_type_User.GUID,
                                                                                        .ID_RelationType = objLocalConfig.OItem_RelationType_secured_by.GUID}}

        Dim objOItem_Result = objDBLevel_Password.GetDataClassRel(searchPasswordClass)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            Dim objOItem_Password = objDBLevel_Password.ClassRels.Select(Function(cls) New clsOntologyItem With {.GUID = cls.ID_Class_Right,
                                                                                                                         .Name = cls.Name_Class_Left,
                                                                                                                         .Type = objLocalConfig.Globals.Type_Class}).FirstOrDefault()

            If Not objOItem_Password Is Nothing Then
                objOItem_Result.OList_Rel = New List(Of clsOntologyItem)
                objOItem_Result.OList_Rel.Add(objOItem_Password)
            End If

        End If
        Return objOItem_Result
    End Function

    Public Function GetOItem(GUID_Item As String, Type_Item As String) As clsOntologyItem
        Return objDBLevel_OItem.GetOItem(GUID_Item, Type_Item)
    End Function

    Public Sub New(LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig


        Initialize()
    End Sub

    Private Sub Initialize()
        objDBLevel_Password = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_User = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_OItem = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_AllowedClass = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_SecuredBy = New OntologyModDBConnector(objLocalConfig.Globals)
    End Sub
End Class
